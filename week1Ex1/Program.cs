﻿using System;
using week1Ex1.model;

namespace week1Ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person1 = new Person("Fiona", "Christchurch", "Thai", "Water");
            Person person2 = new Person("George", "Hamilton", "Pancakes", "Orange Juice");
            Person person3 = new Person("Andrew", "Tokoroa", "Mexican", "Fizzy Water With Lime");

            PrintToScreen(person1);
            PrintToScreen(person2);
            PrintToScreen(person3);
        }
        public static void PrintToScreen(Person person)
        {
            Console.WriteLine($"My name is {person.Name} and I am born in {person.BirthPlace}");
            Console.WriteLine($"I love to eat {person.FavFood} and drink {person.FavDrink}");
            Console.WriteLine();
        }        
    }    
}
