﻿

namespace week1Ex1.model
{
    public class Person
    {
        public string Name { get; set; }
        public string BirthPlace { get; set; }
        public string FavFood { get; set; }
        public string FavDrink { get; set; }
        public Person(string name, string birthplace, string favfood, string favdrink)
        {
            Name = name;
            BirthPlace = birthplace;
            FavFood = favfood;
            FavDrink = favdrink;
        }
    }
}
